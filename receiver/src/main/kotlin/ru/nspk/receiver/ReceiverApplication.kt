package ru.nspk.receiver

import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.core.scheduler.Schedulers
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicInteger

@RestController
@SpringBootApplication
class ReceiverApplication {

    val atomicIntegerWithMono = AtomicInteger(0)

    val log: Log = LogFactory.getLog(ReceiverApplication::class.java)

//    @GetMapping("/",
//        produces = [MediaType.APPLICATION_STREAM_JSON_VALUE],
//        consumes = [MediaType.APPLICATION_STREAM_JSON_VALUE]
//    )
    fun getWithBlock(): Mono<Unit> {
//         имитация блокирующей библиотеки
        TimeUnit.SECONDS.sleep(1)
        return Mono.fromCallable {
            //имитация работы
            TimeUnit.MILLISECONDS.sleep(1000)
            log.info("with mono ${atomicIntegerWithMono.addAndGet(1)}")
        }.subscribeOn(Schedulers.elastic())
    }


    @GetMapping("/",
        produces = [MediaType.APPLICATION_STREAM_JSON_VALUE],
        consumes = [MediaType.APPLICATION_STREAM_JSON_VALUE]
    )
    fun getWithMono(): Mono<Unit> {
        return Mono.fromCallable {
            // имитация блокирующей библиотеки
            TimeUnit.SECONDS.sleep(1)
        }.flatMap {
            //имитация работы
            TimeUnit.MILLISECONDS.sleep(1000)
            Mono.just(log.info("with mono ${atomicIntegerWithMono.addAndGet(1)}"))
        }.subscribeOn(Schedulers.elastic())
    }
}

fun main(args: Array<String>) {
    runApplication<ReceiverApplication>(*args)
}
