package ru.nspk.demo

import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import reactor.core.publisher.Mono
import java.time.Duration

@SpringBootApplication
class ZipDemoApplication

fun main(args: Array<String>) {
    val logger = LoggerFactory.getLogger(ZipDemoApplication::class.java)
    runApplication<ZipDemoApplication>(*args)
    Mono.zip(publisher1(), publisher2())
        .map {tuple ->
            logger.info(tuple.t1.toString())
            logger.info(tuple.t2.toString())
        }.block()
}

private fun publisher1() = Mono.just(listOf(1, 2, 3))

private fun publisher2() = Mono.just(listOf("a", "b", "c")).delayElement(Duration.ofSeconds(5))