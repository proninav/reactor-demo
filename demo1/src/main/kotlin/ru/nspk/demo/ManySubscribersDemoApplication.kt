package ru.nspk.demo

import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import reactor.core.publisher.Mono

@SpringBootApplication
class ManySubscribersDemoApplication

fun main(args: Array<String>) {
    val logger = LoggerFactory.getLogger(ManySubscribersDemoApplication::class.java)
    runApplication<ManySubscribersDemoApplication>(*args)

    println()

    Mono.fromCallable { "test" }
        .flatMap {
            newSubscription()
            Mono.just("afterSubscription")
        }
        .onErrorResume {
            Mono.fromCallable {
                logger.info("in onErrorResume")
                ""
            }
        }
        .block()
}

fun newSubscription() = Mono.error<Exception>(Exception()).subscribe()