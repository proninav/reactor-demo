package ru.nspk.middleware

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Mono
import reactor.core.scheduler.Schedulers
import java.util.concurrent.TimeUnit

@RestController
@SpringBootApplication
class MiddlewareApplication {

    val webClientWithMono = WebClient.builder().baseUrl("http://localhost:9092")
        .build()

    @GetMapping("/",
        produces = [MediaType.APPLICATION_STREAM_JSON_VALUE],
        consumes = [MediaType.APPLICATION_STREAM_JSON_VALUE]
    )
    fun get(): Mono<String> =
        Mono.fromCallable {
            // имитация работы
            TimeUnit.MILLISECONDS.sleep(2000)
        }
            .log()
            .subscribeOn(Schedulers.elastic())
            .flatMap {
                webClientWithMono
                    .get()
                    .accept(MediaType.APPLICATION_STREAM_JSON)
                    .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_STREAM_JSON_VALUE)
                    .retrieve()
                    .bodyToMono(String::class.java)
            }
}

fun main(args: Array<String>) {
    runApplication<MiddlewareApplication>(*args)
}
