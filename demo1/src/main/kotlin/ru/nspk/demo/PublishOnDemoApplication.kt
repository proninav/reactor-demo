package ru.nspk.demo

import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.core.scheduler.Schedulers
import java.util.concurrent.ForkJoinPool

@SpringBootApplication
class PublishOnDemoApplication

fun main(args: Array<String>) {
    val logger = LoggerFactory.getLogger(PublishOnDemoApplication::class.java)
    runApplication<PublishOnDemoApplication>(*args)

    Mono.just(listOf(1, 2, 3, 4, 5))
        .map {
            logger.info(it.toString())
            it
        }
        .publishOn(Schedulers.elastic())
        .map {
            logger.info(it.toString())
            it
        }.block()

    println()

    Mono.just(listOf(1, 2, 3, 4, 5))
        .map {
            logger.info(it.toString())
            it
        }
        .subscribeOn(Schedulers.elastic())
        .map {
            logger.info(it.toString())
            it
        }.block()

    println()

    Flux.fromIterable(listOf(1, 2, 3, 4, 5))
        .subscribeOn(Schedulers.fromExecutor(ForkJoinPool()))
        .map {
            logger.info(it.toString())
            it
        }.blockLast()

    println()

    Flux.fromIterable(listOf(1, 2, 3, 4, 5))
        .parallel()
        .runOn(Schedulers.elastic())
        .flatMap {
            logger.info(it.toString())
            Flux.just(it)
        }
        .sequential()
        .blockLast()
}