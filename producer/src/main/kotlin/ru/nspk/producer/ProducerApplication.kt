package ru.nspk.producer

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Flux
import reactor.core.scheduler.Schedulers
import kotlin.random.Random

@SpringBootApplication
class ProducerApplication

val webClient = WebClient
    .builder()
    .baseUrl("http://localhost:9091")
    .build()

fun main(args: Array<String>) {
    runApplication<ProducerApplication>(*args)

    Flux.generate<Int> { it.next(Random.nextInt()) }
        .log()
        .subscribeOn(Schedulers.elastic())
        .flatMap {
            webClient
                .get()
                .accept(MediaType.APPLICATION_STREAM_JSON)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_STREAM_JSON_VALUE)
                .exchange()
        }
        .subscribe()
}
