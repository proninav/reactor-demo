package ru.nspk.demo

import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import reactor.core.publisher.Mono

@SpringBootApplication
class ErrorHandlingDemoApplication

fun main(args: Array<String>) {
    val logger = LoggerFactory.getLogger(ErrorHandlingDemoApplication::class.java)
    runApplication<ErrorHandlingDemoApplication>(*args)

    println()

    Mono.fromCallable {
        throw Exception()
        "fromCallable"
    }.onErrorReturn("Default value")
        .map {
            logger.info(it)
        }
        .block()

    println()

    Mono.fromCallable {
        throw Exception()
        "fromCallable"
    }.onErrorResume {
        logger.info("onErrorResume")
        Mono.fromCallable {
            logger.info("new publisher")
            "new value"
        }
    }
        .map {
            logger.info(it)
        }.block()

    println()

    Mono.just("mono error test")
        .flatMap {
            if (true) Mono.error<Exception>(Exception())
            else Mono.just("flatMap")

        }.onErrorResume {
            Mono.just("new publisher")
        }
        .map {
            logger.info("$it")
            "qqq"
        }.block()

    println()

    Mono.fromCallable {
        throw Exception()
        "fromCallable"
    }.doOnError {
        logger.info("doOnError")
        Mono.just("Just")
    }
        .map {
            logger.info(it)
        }
        .block()
}