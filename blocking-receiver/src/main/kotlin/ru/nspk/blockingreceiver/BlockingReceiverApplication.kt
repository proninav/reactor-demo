package ru.nspk.blockingreceiver

import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicInteger

@RestController
@SpringBootApplication
class BlockingReceiverApplication {

    val atomicIntegerWithoutMono = AtomicInteger(0)

    val log: Log = LogFactory.getLog(BlockingReceiverApplication::class.java)

    @GetMapping("/")
    fun get(): String {
        atomicIntegerWithoutMono.addAndGet(1)
        log.info("without mono ${atomicIntegerWithoutMono.get()}")
        TimeUnit.SECONDS.sleep(5)
        return "success"
    }
}

fun main(args: Array<String>) {
    runApplication<BlockingReceiverApplication>(*args)
}
